import React from 'react'
import Reflux from 'reflux'
import { Route, Link } from 'react-router-dom'

import Store from './store'
import Actions from './actions'

import Home from '../home/index.jsx'
import About from '../about/index.jsx'
import Contact from '../contact/index.jsx'

export default class APP extends Reflux.Component
{
	constructor(props)
	{
		super(props);
		this.store = Store;
	}
	getSizeWindows() {
    var myWidth = 0, myHeight = 0;
    if( typeof( window.innerWidth ) == 'number' ) {
      //Non-IE
      myWidth = window.innerWidth;
      myHeight = window.innerHeight;
    } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
      //IE 6+ in 'standards compliant mode'
      myWidth = document.documentElement.clientWidth;
      myHeight = document.documentElement.clientHeight;
    } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
      //IE 4 compatible
      myWidth = document.body.clientWidth;
      myHeight = document.body.clientHeight;
    }
    return {
      width: myWidth,
      height: myHeight
    }
  }
  resizeWindows () {
    var obj = document.getElementsByClassName('site-wrapper')
    if(obj){
      let size = this.getSizeWindows()
      var el = obj[0]
      el.style.height = (size.height-5)+'px'
    }
  }
  componentDidMount () {
    this.resizeWindows()
    document.addEventListener('resize', this.resizeWindows, true)
  }
  componentWillUnmount () {
    document.removeEventListener('resize', this.resizeWindows, true)
  }
  isActive (pos) {
    return pos === this.state.active ? 'active' : ''
  }
	render()
	{
		return (
        <div className="site-wrapper">
          <div className="site-wrapper-inner">
            <div className="cover-container">
              <div className="masthead clearfix">
                <div className="inner">
                  <h3 className="masthead-brand">{this.state.title}</h3>
                  <nav>
                    <ul className="nav masthead-nav">
                      <li className={this.isActive(0)}><Link to="/home">Home</Link></li>
                      <li className={this.isActive(1)}><Link to="/features">About</Link></li>
                      <li className={this.isActive(2)}><Link to="/contact">Contact</Link></li>
                    </ul>
                  </nav>
                </div>
              </div>

              <div>
                <Route path='/home' component={Home} />
                <Route path='/features' component={About} />
                <Route path='/contact' component={Contact} />
              </div>

              <div className="mastfoot">
                <div className="inner">
                  <p>Cover template for <a href="http://getbootstrap.com">Bootstrap</a>, by <a href="https://twitter.com/mdo">@mdo</a>.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
    )
	}
}