import React from 'react'
import Reflux from 'reflux'
import Store from './store'
import Actions from './actions'

import LayoutBaseAction from '../layoutBase/actions.js'

export default class About extends Reflux.Component
{
	constructor(props)
	{
		super(props);
		this.store = Store;
	}
	componentDidMount () {
    LayoutBaseAction.setActive(1)
		LayoutBaseAction.setTitle('ABOUT')
  }
	render()
	{
		return (
      <div className="inner cover">
        <h1 className="cover-heading">About.</h1>
        <p className="lead">About About About About About About About About About About About.</p>
      </div>
    )
	}
}