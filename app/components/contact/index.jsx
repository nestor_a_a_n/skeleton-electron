import React from 'react'
import Reflux from 'reflux'
import Store from './store'
import Actions from './actions'

import LayoutBaseAction from '../layoutBase/actions.js'

export default class Contact extends Reflux.Component
{
	constructor(props)
	{
		super(props);
		this.store = Store;
	}
	componentDidMount () {
    LayoutBaseAction.setActive(2)
		LayoutBaseAction.setTitle('CONTACT')
  }
	render()
	{
		return (
      <div className="inner cover">
        <h1 className="cover-heading">Contact.</h1>
        <p className="lead">Contact Contact Contact Contact Contact Contact Contact Contact Contact Contact Contact.</p>
      </div>
    )
	}
}