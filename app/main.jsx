
import React from 'react';
import {render} from 'react-dom'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import LayoutBase from './components/layoutBase/index.jsx'

render(
  ( 
    <Router>
      <Route path="/" component={LayoutBase} />
    </Router>
  ), 
  document.getElementById('root') 
);